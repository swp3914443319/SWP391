USE [Thehieu01]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 12/05/2024 9:58:42 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Account](
	[userId] [int] IDENTITY(1,1) NOT NULL,
	[username] [nvarchar](255) NULL,
	[password] [nvarchar](255) NULL,
	[statusAccountId] [int] NULL,
	[email] [nvarchar](255) NULL,
	[phone] [nvarchar](255) NULL,
	[address] [nvarchar](255) NULL,
	[roleId] [int] NULL,
	[created_date] [datetime] NULL,
	[avata] [nvarchar](255) NULL,
	[classId] [int] NULL,
 CONSTRAINT [PK_Account] PRIMARY KEY CLUSTERED 
(
	[userId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Answer]    Script Date: 12/05/2024 9:58:42 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Answer](
	[answerId] [int] NOT NULL,
	[numberSentences] [int] NULL,
	[answer] [nvarchar](50) NULL,
	[quizId] [int] NULL,
	[userId] [int] NULL,
	[classId] [int] NULL,
 CONSTRAINT [PK_Answer] PRIMARY KEY CLUSTERED 
(
	[answerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Class]    Script Date: 12/05/2024 9:58:42 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Class](
	[classId] [int] IDENTITY(1,1) NOT NULL,
	[className] [nvarchar](255) NULL,
	[quizId] [int] NULL,
	[classCode] [int] NULL,
	[roleId] [int] NULL,
 CONSTRAINT [PK_Class] PRIMARY KEY CLUSTERED 
(
	[classId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Comment_In_Post]    Script Date: 12/05/2024 9:58:42 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Comment_In_Post](
	[commentId] [int] IDENTITY(1,1) NOT NULL,
	[userId] [int] NULL,
	[decriptionComment] [nvarchar](255) NULL,
	[roleId] [int] NULL,
	[postId] [int] NULL,
	[quizId] [int] NULL,
 CONSTRAINT [PK_Comment_In_Post] PRIMARY KEY CLUSTERED 
(
	[commentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NumberOfQuestion]    Script Date: 12/05/2024 9:58:42 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NumberOfQuestion](
	[numberOfQuesionId] [int] NOT NULL,
	[questionNumber] [int] NULL,
 CONSTRAINT [PK_NumberOfQuestion] PRIMARY KEY CLUSTERED 
(
	[numberOfQuesionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Post]    Script Date: 12/05/2024 9:58:42 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Post](
	[postId] [int] IDENTITY(1,1) NOT NULL,
	[classId] [int] NULL,
	[roleId] [int] NULL,
	[userId] [int] NULL,
	[quizId] [int] NULL,
	[descriptionPost] [nvarchar](255) NULL,
	[commentId] [int] NULL,
 CONSTRAINT [PK_Post] PRIMARY KEY CLUSTERED 
(
	[postId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Question]    Script Date: 12/05/2024 9:58:42 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Question](
	[quesId] [int] NOT NULL,
	[typeId] [int] NULL,
	[numberOfQuesionId] [int] NULL,
	[userId] [int] NULL,
	[quizId] [int] NULL,
	[roleId] [int] NULL,
	[answerId] [int] NULL,
	[decription] [nvarchar](255) NULL,
 CONSTRAINT [PK_Question] PRIMARY KEY CLUSTERED 
(
	[quesId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Quiz]    Script Date: 12/05/2024 9:58:42 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Quiz](
	[quizId] [int] IDENTITY(1,1) NOT NULL,
	[title] [nvarchar](255) NULL,
	[img] [nvarchar](255) NULL,
	[status] [bit] NULL,
	[rateGPA] [float] NULL,
	[timeStart] [time](7) NULL,
	[timeEnd] [time](7) NULL,
	[typeId] [int] NULL,
	[userId] [int] NULL,
	[description] [nvarchar](255) NULL,
	[quesId] [int] NULL,
 CONSTRAINT [PK_Quiz] PRIMARY KEY CLUSTERED 
(
	[quizId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RoleAccount]    Script Date: 12/05/2024 9:58:42 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RoleAccount](
	[roleId] [int] NOT NULL,
	[roleName] [nvarchar](255) NULL,
 CONSTRAINT [PK_RoleAccount] PRIMARY KEY CLUSTERED 
(
	[roleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Status]    Script Date: 12/05/2024 9:58:42 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Status](
	[statusId] [int] NOT NULL,
	[status] [nvarchar](255) NULL,
 CONSTRAINT [PK_Status] PRIMARY KEY CLUSTERED 
(
	[statusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Type_Test]    Script Date: 12/05/2024 9:58:42 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Type_Test](
	[typeId] [int] NOT NULL,
	[testName] [nvarchar](255) NULL,
	[numberOfQuesionId] [int] NULL,
 CONSTRAINT [PK_Type_Test] PRIMARY KEY CLUSTERED 
(
	[typeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [FK_Account_RoleAccount] FOREIGN KEY([roleId])
REFERENCES [dbo].[RoleAccount] ([roleId])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_RoleAccount]
GO
ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [FK_Account_Status] FOREIGN KEY([statusAccountId])
REFERENCES [dbo].[Status] ([statusId])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_Status]
GO
ALTER TABLE [dbo].[Answer]  WITH CHECK ADD  CONSTRAINT [FK_Answer_Account] FOREIGN KEY([userId])
REFERENCES [dbo].[Account] ([userId])
GO
ALTER TABLE [dbo].[Answer] CHECK CONSTRAINT [FK_Answer_Account]
GO
ALTER TABLE [dbo].[Answer]  WITH CHECK ADD  CONSTRAINT [FK_Answer_Class] FOREIGN KEY([classId])
REFERENCES [dbo].[Class] ([classId])
GO
ALTER TABLE [dbo].[Answer] CHECK CONSTRAINT [FK_Answer_Class]
GO
ALTER TABLE [dbo].[Answer]  WITH CHECK ADD  CONSTRAINT [FK_Answer_Quiz] FOREIGN KEY([quizId])
REFERENCES [dbo].[Quiz] ([quizId])
GO
ALTER TABLE [dbo].[Answer] CHECK CONSTRAINT [FK_Answer_Quiz]
GO
ALTER TABLE [dbo].[Class]  WITH CHECK ADD  CONSTRAINT [FK_Class_Quiz] FOREIGN KEY([quizId])
REFERENCES [dbo].[Quiz] ([quizId])
GO
ALTER TABLE [dbo].[Class] CHECK CONSTRAINT [FK_Class_Quiz]
GO
ALTER TABLE [dbo].[Class]  WITH CHECK ADD  CONSTRAINT [FK_Class_RoleAccount] FOREIGN KEY([roleId])
REFERENCES [dbo].[RoleAccount] ([roleId])
GO
ALTER TABLE [dbo].[Class] CHECK CONSTRAINT [FK_Class_RoleAccount]
GO
ALTER TABLE [dbo].[Comment_In_Post]  WITH CHECK ADD  CONSTRAINT [FK_Comment_In_Post_Account] FOREIGN KEY([userId])
REFERENCES [dbo].[Account] ([userId])
GO
ALTER TABLE [dbo].[Comment_In_Post] CHECK CONSTRAINT [FK_Comment_In_Post_Account]
GO
ALTER TABLE [dbo].[Comment_In_Post]  WITH CHECK ADD  CONSTRAINT [FK_Comment_In_Post_Post] FOREIGN KEY([postId])
REFERENCES [dbo].[Post] ([postId])
GO
ALTER TABLE [dbo].[Comment_In_Post] CHECK CONSTRAINT [FK_Comment_In_Post_Post]
GO
ALTER TABLE [dbo].[Comment_In_Post]  WITH CHECK ADD  CONSTRAINT [FK_Comment_In_Post_Question] FOREIGN KEY([quizId])
REFERENCES [dbo].[Question] ([quesId])
GO
ALTER TABLE [dbo].[Comment_In_Post] CHECK CONSTRAINT [FK_Comment_In_Post_Question]
GO
ALTER TABLE [dbo].[Comment_In_Post]  WITH CHECK ADD  CONSTRAINT [FK_Comment_In_Post_RoleAccount] FOREIGN KEY([roleId])
REFERENCES [dbo].[RoleAccount] ([roleId])
GO
ALTER TABLE [dbo].[Comment_In_Post] CHECK CONSTRAINT [FK_Comment_In_Post_RoleAccount]
GO
ALTER TABLE [dbo].[Post]  WITH CHECK ADD  CONSTRAINT [FK_Post_Account] FOREIGN KEY([userId])
REFERENCES [dbo].[Account] ([userId])
GO
ALTER TABLE [dbo].[Post] CHECK CONSTRAINT [FK_Post_Account]
GO
ALTER TABLE [dbo].[Post]  WITH CHECK ADD  CONSTRAINT [FK_Post_Class] FOREIGN KEY([classId])
REFERENCES [dbo].[Class] ([classId])
GO
ALTER TABLE [dbo].[Post] CHECK CONSTRAINT [FK_Post_Class]
GO
ALTER TABLE [dbo].[Post]  WITH CHECK ADD  CONSTRAINT [FK_Post_Comment_In_Post] FOREIGN KEY([commentId])
REFERENCES [dbo].[Comment_In_Post] ([commentId])
GO
ALTER TABLE [dbo].[Post] CHECK CONSTRAINT [FK_Post_Comment_In_Post]
GO
ALTER TABLE [dbo].[Post]  WITH CHECK ADD  CONSTRAINT [FK_Post_Quiz] FOREIGN KEY([quizId])
REFERENCES [dbo].[Quiz] ([quizId])
GO
ALTER TABLE [dbo].[Post] CHECK CONSTRAINT [FK_Post_Quiz]
GO
ALTER TABLE [dbo].[Post]  WITH CHECK ADD  CONSTRAINT [FK_Post_RoleAccount] FOREIGN KEY([roleId])
REFERENCES [dbo].[RoleAccount] ([roleId])
GO
ALTER TABLE [dbo].[Post] CHECK CONSTRAINT [FK_Post_RoleAccount]
GO
ALTER TABLE [dbo].[Question]  WITH CHECK ADD  CONSTRAINT [FK_Question_Account] FOREIGN KEY([userId])
REFERENCES [dbo].[Account] ([userId])
GO
ALTER TABLE [dbo].[Question] CHECK CONSTRAINT [FK_Question_Account]
GO
ALTER TABLE [dbo].[Question]  WITH CHECK ADD  CONSTRAINT [FK_Question_Answer] FOREIGN KEY([answerId])
REFERENCES [dbo].[Answer] ([answerId])
GO
ALTER TABLE [dbo].[Question] CHECK CONSTRAINT [FK_Question_Answer]
GO
ALTER TABLE [dbo].[Question]  WITH CHECK ADD  CONSTRAINT [FK_Question_Quiz] FOREIGN KEY([quizId])
REFERENCES [dbo].[Quiz] ([quizId])
GO
ALTER TABLE [dbo].[Question] CHECK CONSTRAINT [FK_Question_Quiz]
GO
ALTER TABLE [dbo].[Question]  WITH CHECK ADD  CONSTRAINT [FK_Question_RoleAccount] FOREIGN KEY([roleId])
REFERENCES [dbo].[RoleAccount] ([roleId])
GO
ALTER TABLE [dbo].[Question] CHECK CONSTRAINT [FK_Question_RoleAccount]
GO
ALTER TABLE [dbo].[Question]  WITH CHECK ADD  CONSTRAINT [FK_Question_Type_Test] FOREIGN KEY([typeId])
REFERENCES [dbo].[Type_Test] ([typeId])
GO
ALTER TABLE [dbo].[Question] CHECK CONSTRAINT [FK_Question_Type_Test]
GO
ALTER TABLE [dbo].[Quiz]  WITH CHECK ADD  CONSTRAINT [FK_Quiz_Account] FOREIGN KEY([quizId])
REFERENCES [dbo].[Account] ([userId])
GO
ALTER TABLE [dbo].[Quiz] CHECK CONSTRAINT [FK_Quiz_Account]
GO
ALTER TABLE [dbo].[Quiz]  WITH CHECK ADD  CONSTRAINT [FK_Quiz_Question] FOREIGN KEY([quesId])
REFERENCES [dbo].[Question] ([quesId])
GO
ALTER TABLE [dbo].[Quiz] CHECK CONSTRAINT [FK_Quiz_Question]
GO
ALTER TABLE [dbo].[Quiz]  WITH CHECK ADD  CONSTRAINT [FK_Quiz_Type_Test] FOREIGN KEY([typeId])
REFERENCES [dbo].[Type_Test] ([typeId])
GO
ALTER TABLE [dbo].[Quiz] CHECK CONSTRAINT [FK_Quiz_Type_Test]
GO
ALTER TABLE [dbo].[Type_Test]  WITH CHECK ADD  CONSTRAINT [FK_Type_Test_NumberOfQuestion] FOREIGN KEY([numberOfQuesionId])
REFERENCES [dbo].[NumberOfQuestion] ([numberOfQuesionId])
GO
ALTER TABLE [dbo].[Type_Test] CHECK CONSTRAINT [FK_Type_Test_NumberOfQuestion]
GO
